#ifndef UI_H
#define UI_H

#include <curses.h>
#include <stdbool.h>
#include <string.h>

WINDOW *ui_init();
void ui_printtext(WINDOW *window, char *text, bool geminimap);
bool ui_popup_decision(char *msg, char *option1, char *option2);
void ui_popup_big(char *status, char *msg, char *msg2);
void ui_popup_input(char *input, char *text);
void ui_popup(char *status, char *msg);
void ui_deinit();

#endif
