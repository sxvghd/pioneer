#ifndef TLS_H
#define TLS_H

#include <stdio.h>
#include "mbedtls/net_sockets.h"
#include "mbedtls/debug.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/error.h"
#include "mbedtls/certs.h"

typedef struct {
	mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_ssl_context ssl;
    mbedtls_ssl_config conf;
    mbedtls_x509_crt cacert;	
	mbedtls_net_context server_fd;
} tlsconfig;

int tls_init(tlsconfig *conf_struct);
void tls_deinit(tlsconfig *conf_struct);
int tls_connect(tlsconfig *conf_struct, char *host, char *port);
int tls_disconnect(tlsconfig *conf_struct);
int tls_write(tlsconfig *conf_struct, char *buf);
char *tls_read(tlsconfig *conf_struct);
char *tls_read_delim(tlsconfig *conf_struct, char delim);
#endif
