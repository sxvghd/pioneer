#include <limits.h>
#include <stddef.h>
#include "ui.h"
#include "util.h"

WINDOW *ui_init()
{
	initscr();
	cbreak();
	noecho();
	WINDOW *window = newpad(LINES+1024,COLS);
	keypad(window,true);
	refresh();
	//clearok(window,true);
	return window;
}

void ui_printtext(WINDOW *window, char *text, bool geminimap)
{
	int line = 1;
	char *temp = strsep(&text,"\n");
	werase(window);
	while(text != NULL)
	{
		if(geminimap && strlen(temp) > 2 && temp[0] == '=' && temp[1] == '>')
		{
			//overengineered, but works!
			char *final = malloc(sizeof(char)*4);
			strcpy(final,"=> ");
			temp += 2;
			//get through them whitespaces
			if(temp[0] == ' ' || temp[0] == '\t')
			{
				while(temp[0] == ' ' || temp[0] == '\t')
					temp++;
			}
			if(strrchr(temp,'\t') == NULL && strrchr(temp,' ') == NULL)
				goto print;
			//get through them links
			int count = 0;
			while(temp[0] != ' ' && temp[0] != '\t')
			{
				if(temp[0] == ':' && strncmp((temp-count),"gemini",count) != 0)
				{
					temp -= count;
					final = realloc(final,sizeof(char)*(count+7));
					strcat(final,"[");
					strncat(final,temp,count);
					strcat(final,"] ");
					temp += count;
				}
				count++;
				temp++;
			}
			//get through them whitespaces
			if(temp[0] == ' ' || temp[0] == '\t')
			{
				while(temp[0] == ' ' || temp[0] == '\t')
					temp++;
			}
			print:
			final = realloc(final,sizeof(char)*(strlen(final)+strlen(temp)+1));
			strcat(final,temp);
			mvwaddstr(window,line,1,final);
			free(final);
		}
		else
			mvwaddstr(window,line,1,temp);
		line++;
		temp = strsep(&text,"\n");
	}
	return;
}

bool ui_popup_decision(char *msg, char *option1, char *option2)
{
	WINDOW *popup = newwin(6,COLS/2,(LINES/2)-2,COLS/4);
	keypad(popup,true);
    box(popup,'|','-');
	mvwaddstr(popup,1,(LINES/2)-(strlen(msg)/2),msg);
	mvwaddstr(popup,3,1,option1);
	mvwaddstr(popup,4,1,option2);
	mvwchgat(popup,3,1,(COLS/2)-1,A_REVERSE,0,NULL);
	wchar_t cmd = 0;
	bool state = 1;
	while(1)
	{
		wrefresh(popup);
		cmd = wgetch(popup);
		switch(cmd)
		{
			case KEY_NPAGE:
                if(state == true)
                {
                    mvwchgat(popup,3,1,(COLS/2)-1,NULL,0,NULL);
                    state = 0;
                    mvwchgat(popup,4,1,(COLS/2)-1,A_REVERSE,0,NULL);
                }
                break;
            case KEY_PPAGE:
                if(state == false)
                {
                    mvwchgat(popup,4,1,(COLS/2)-1,NULL,0,NULL);
                    state = 1;
                    mvwchgat(popup,3,1,(COLS/2)-1,A_REVERSE,0,NULL);
                }
                break;
			case '\n':
				delwin(popup);
				return state;
		}
	}
	delwin(popup);
	return state;
}

void ui_popup_input(char *input, char *text)
{
	WINDOW *popup = newwin(5,(COLS/8)*6,(LINES/2)-2,COLS/8);
	box(popup,'|','-');
	mvwaddstr(popup,1,((COLS/8)*3)-(strlen(text)/2),text);
	mvwaddstr(popup,3,1,"=> ");
	wrefresh(popup);
    echo();
	wmove(popup,3,4);
    wgetnstr(popup,input,1024);
    noecho();
	delwin(popup);
}

void ui_popup(char *status, char *msg)
{
	WINDOW *popup = newwin(5,COLS/2,(LINES/2)-2,COLS/4);
	box(popup,'|','-');
	mvwaddstr(popup,1,(LINES/2)-(strlen(status)/2),status);
	mvwaddstr(popup,2,(LINES/2)-(strlen(msg)/2),msg);
	mvwaddstr(popup,4,1,"Press any key...");
	wrefresh(popup);
	wgetch(popup);
	delwin(popup);
}

void ui_popup_big(char *status, char *msg, char *msg2)
{
	WINDOW *popup = newwin(6,(COLS/8)*6,(LINES/2)-2,COLS/8);
	box(popup,'|','-');
	mvwaddstr(popup,1,(COLS/8*3)-(strlen(status)/2),status);
	mvwaddstr(popup,2,(COLS/8*3)-(strlen(msg)/2),msg);
	mvwaddstr(popup,4,(COLS/8*3)-(strlen(msg2)/2),msg2);
	mvwaddstr(popup,5,1,"Press any key...");
	wrefresh(popup);
	wgetch(popup);
	delwin(popup);
}

void ui_deinit()
{
	nocbreak();
	echo();
	endwin();
	return;
}
