#ifndef UTIL_H
#define UTIL_H

#include "tls.h"
#include "ui.h"

typedef struct {
	int num;
	int *line_num;
	char **line_url;
} link_info;

typedef struct {
	struct link_history *prev;
	char url[1024];
} link_history;

int str_count(char *str, char c);
char *get_host_from_url(char *url);
char *get_filename_from_url(char *url);
char *get_response_meta(char *line);

void download_file(tlsconfig *conf_tls, char url[1024], WINDOW *root_pad);

link_info get_link_info(char *response, char *host, char *current_url);
void free_link_info(link_info *linfo);

void push_link_history(link_history **history, char *url);
void pop_link_history(link_history **history);
void free_link_history(link_history **history);

//standard on BSD (maybe even MacOS), Linux, so compatibility
#ifdef _WIN32
char *strsep(char **stringp, const char *delim);
#endif

#endif
