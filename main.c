#include "ui.h"
#include "tls.h"
#include "util.h"

link_history *history;
int link_cur = 0;
int scroll_cur = 0;
#define popup_break(one,two) {\
			ui_popup(one,two);\
			return;\
		}

//beware: this is the ugliest function in this whole project, here be dragons and stuff
void url_cycle(tlsconfig *conf_tls, char url[1024], WINDOW *root_pad, link_info *linfo, bool add_link)
{
	char buf[1024];
	if(strncmp(url,"gemini://",9) != 0)
	{
		ui_popup_big("ERROR","Invalid URL",url);
		return;
	}
	memcpy(buf,url,sizeof(char)*1024);
	tls_disconnect(conf_tls);
	int err = tls_connect(conf_tls,get_host_from_url(buf),"1965");
	switch(err)
	{
		case 0:
			if(str_count(url,'/') <= 2)
				strcat(url,"/");
			if(strchr(url,'\n') == NULL)
				strcat(url,"\r\n");
			tls_write(conf_tls, url);
			char *header = tls_read_delim(conf_tls,'\n');
			switch(header[0])
			{
				case '1': //input
					memset(&buf,0,sizeof(char)*1024);
					ui_popup_input(&buf,"Enter your input:");
					if(strlen(url)+strlen(buf) > 1023)
						popup_break("ERROR","String too long");
					url[strlen(url)-1] = '\0';
					url[strlen(url)-1] = '\0';
					strcat(url,"?");
					strcat(url,buf);
					strcat(url,"\r\n");
					url_cycle(conf_tls,url,root_pad,linfo,add_link);
				case '2':
					break;
				case '3': //redirect
					if(3>2){}
					char *url_ptr = header+2;
					while(url_ptr[0] == ' ' || url_ptr[0] == '\t')
						url_ptr++;
					char new_url[1024];
					memset(&new_url,0,sizeof(char)*1024);
					memcpy(new_url,url_ptr,sizeof(char)*strlen(url_ptr));
					url_ptr[strlen(url_ptr)-1] = '\0';
					url_ptr[strlen(url_ptr)-1] = '\0';
					ui_popup_big("WARNING", "Redirecting to:", url_ptr);
					url_cycle(conf_tls,new_url,root_pad,linfo,add_link);
					return;
				case '4':
					popup_break("ERROR","Temporary request failure");
				case '5':
					popup_break("ERROR","Permanent request failure");
				case '6': //cert required
					popup_break("ERROR","Not implemented yet");
				default:
					popup_break("ERROR","Unknown error");
			}
			if(header[0] != '2')
				return;
			char *meta = get_response_meta(header);
			bool parse = false;
			//if not plaintext, try to download it
			if(strncmp(meta,"text",4) != 0)
			{
				download_file(conf_tls,url,root_pad);
				return;
			}
			//if geminimap, parse
			if(strncmp(meta,"text/gemini",11) == 0)
				parse = true;
			free(header);

			if(add_link)
				push_link_history(&history,url);
			
			char *response = tls_read(conf_tls);
			if(response == NULL)
				response = calloc(1,sizeof(char));
			char *resp_back = malloc(sizeof(char)*strlen(response));
			memcpy(resp_back,response,sizeof(char)*strlen(response));
						
			ui_printtext(root_pad, response, parse);
			//get url info
			if((*linfo).line_num != NULL)
				free_link_info(linfo);
			
			char host[1024];
            memset(&host,'\0',sizeof(char)*1024);
            int offset = 10;
            while(url[offset] != '/' && url[offset] != '\0')
                offset++;
            strncpy(&host,url,offset);
			(*linfo) = get_link_info(resp_back,host,&(history->url));
			free(response);
			free(resp_back);
			if((*linfo).line_num != NULL)
				mvwchgat(root_pad,(*linfo).line_num[0]+1,1,COLS-2,A_REVERSE,0,NULL);
			link_cur = 0;
			scroll_cur = 0;
			break;
		case 1:
			popup_break("ERROR","TCP connect error");
		case 2:
			popup_break("ERROR","TLS config setup error");
		case 3:
			popup_break("ERROR","TLS setup error");
		case 4:
			popup_break("ERROR","Hostname error");
		case 5:
			popup_break("ERROR","TLS handshake error");
	}
}

int main()
{
	//init
	tlsconfig conf_tls;
	if(tls_init(&conf_tls) != 0)
		return 1;
	WINDOW *root_pad = ui_init();
	wchar_t cmd = 0;
	//(limit included in request spec)
	char input[1024];
	link_info linfo = {0,NULL,NULL};
	//set up history struct
	history = calloc(1,sizeof(link_history));
	history->prev = NULL;

	//main loop
	while(1)
	{
		//refresh();
		prefresh(root_pad,scroll_cur,0,0,0,LINES-1,COLS-1);
		cmd = wgetch(root_pad);
		switch(cmd)
		{	
			//URL input
			case 'g':
				ui_popup_input(&input,"Input URL:");
				url_cycle(&conf_tls,input,root_pad,&linfo,true);
				break;
			case KEY_NPAGE:
				if(linfo.line_url != NULL && link_cur < (linfo.num-1))
				{
					mvwchgat(root_pad,linfo.line_num[link_cur]+1,1,COLS-2,NULL,0,NULL);
					link_cur++;
					mvwchgat(root_pad,linfo.line_num[link_cur]+1,1,COLS-2,A_REVERSE,0,NULL);
				}
				break;
			case KEY_PPAGE:
				if(linfo.line_url != NULL && link_cur > 0)
				{
					mvwchgat(root_pad,linfo.line_num[link_cur]+1,1,COLS-2,NULL,0,NULL);
					link_cur--;
					mvwchgat(root_pad,linfo.line_num[link_cur]+1,1,COLS-2,A_REVERSE,0,NULL);
				}
				break;
			case '\n':
				if(linfo.line_url != NULL)
				{
					strcpy(&input,linfo.line_url[link_cur]);
					url_cycle(&conf_tls,input,root_pad,&linfo,true);
				}
				break;
			case KEY_UP:
				scroll_cur = (scroll_cur == 0 ? scroll_cur : scroll_cur-1);
				break;
			case KEY_DOWN:
				scroll_cur++;
				break;
			#ifdef _WIN32
			case '\b':
			#else
			case KEY_BACKSPACE:
			#endif
				if(history->prev != NULL)
				{
					pop_link_history(&history);
					memset(&input,'\0',1024);
					strncpy(&input,&(history->url),strlen(history->url));
					url_cycle(&conf_tls,input,root_pad,&linfo,false);
				}
				else
					ui_popup("WARNING","Can't go back any further");
				break;
			case 'r':
				refresh();
				prefresh(root_pad,scroll,0,0,0,LINES-1,COLS-1);
				break;
			//exit
			case 'q':
				goto exit;
				break;
		}
	}

	//deinit & exit
	exit:
	if(linfo.line_num != NULL)
		free_link_info(&linfo);
	free_link_history(&history);
	ui_deinit();
	tls_deinit(&conf_tls);
	return 0;
}
