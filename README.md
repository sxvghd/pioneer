# pioneer

A curses based gemini client written in C

**Warning**: this project is abandoned and completely broken (the Gemini protocol has moved a long way since 0.9.x). 

I may come back to work on this, but in all honesty this was a real learning experience and needs a rewrite instead of just some bugfixes.

### Dependencies
*  some curses implementation (tested with ncurses and pdcurses)
*  mbedtls

### Keybindings
| key | action |
| ------ | ------ |
| up | scroll up |
| down | scroll down |
| page up | choose previous link |
| page dn | choose next link |
| return | follow a link |
| backspace | previous page |
| g | input a url |
| q | quit |
