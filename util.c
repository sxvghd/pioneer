#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

#ifdef _WIN32
char* strsep(char **stringp, const char *delim)
{
    char *start = *stringp, *p = start ? strpbrk(start, delim) : NULL;
    if (!p)
        *stringp = NULL;
    else {
        *p = 0;
        *stringp = p + 1;
    }

    return start;
}
#endif

int str_count(char *str, char c)
{
	int max = strlen(str), count = 0;
	for(int i = 0; i < max; i++)
		if(str[i] == c)
			count++;
	return count;
}

char *get_host_from_url(char *url)
{
	url += 9;
	return strsep(&url,"/");
}

char *get_filename_from_url(char *url)
{
	int beg, end = strlen(url)-1, len;
	if(url[end] == '/')
		end--;
	beg = end;
	while(url[beg] != '/')
		beg--;
	len = end-beg;
	beg++;
	char *filename = calloc(len+1,sizeof(char));
	strncpy(filename,(url+beg),len);
	return filename;
}

char *get_response_meta(char *line)
{
	char *meta = line+2;
	while(meta[0] == '\t' || meta[0] == ' ')
		meta++;
	return meta;
}

char *get_url(char *line)
{
	//prepare uranus
    line += 2;
	//trim back
	while(line[0] == ' ' || line[0] == '\t')
    	line++;
	int i = 0;
	while(line[i] != ' ' && line[i] != '\t' && line[i] != '\n'){i++;}
    char *url = malloc(sizeof(char)*(i+2));
	strncpy(url,line,i);
	url[i] = '\0';
	/*if(line[2] == ' ' || line[2] == '\t')
    {
		//TODO: temp, use line[2] and cast as a const char* or sth
        strsep(&url," ");
        url = strsep(&url," ");
    }
    else
        url += 2;
    if(strchr(url,' ') != NULL)
        url = strsep(&url," ");*/
    return url;	
}

void download_file(tlsconfig *conf_tls, char url[1024], WINDOW *root_pad)
{
	if(!ui_popup_decision("Cannot open file. Download?","Yes","No"))
    	return;
    char path[1024];
    refresh();
    wrefresh(root_pad);
    ui_popup_input(&path,"Input download path:");

    //WINDOW
    WINDOW *popup = newwin(5,COLS/2,(LINES/2)-2,COLS/4);
    box(popup,'|','-');
    mvwaddstr(popup,1,(LINES/2)-7,"Downloading...");
    wrefresh(popup);
    FILE *file = fopen(&path,"wb");
    if(file == NULL)
	{
    	ui_popup("ERROR","Error creating/accessing file");
		return;
	}
    tls_download(conf_tls,file);
    fclose(file);
    mvwaddstr(popup,1,(LINES/2)-7,"Downloaded!   ");
    wrefresh(popup);
    //wgetch(popup);
	getch();
    delwin(popup);
    return;
}

link_info get_link_info(char *response, char *host, char *current_url)
{
	link_info links;
	links.num = 0;
	links.line_num = NULL;
	links.line_url = NULL;
	char *line = NULL;
	int trigger = 0, line_count = 0; 
	do
	{
		line = strsep(&response,"\n");
		if(line != NULL)
		{
			if(trigger > 0)
				trigger = 0;
			if(strlen(line) > 2 && line[0] == '=' && line[1] == '>')
			{
				links.line_num = realloc(links.line_num,sizeof(int)*(links.num+1));
				links.line_num[links.num] = line_count;
				links.line_url = realloc(links.line_url,sizeof(char*)*(links.num+1));
				char *url = get_url(line);
				if(url[0] == '/')
				{
					links.line_url[links.num] = calloc(sizeof(char),strlen(url)+strlen(host)+1);
					strncpy(links.line_url[links.num],host,strlen(host));
					strcat(links.line_url[links.num],url);
				}
				else
				{
					if(strchr(url,':') != NULL)
					{
						links.line_url[links.num] = calloc(sizeof(char),strlen(url)+1);
						strncpy(links.line_url[links.num],url,strlen(url)+1);
					}
					else
					{
						links.line_url[links.num] = calloc(sizeof(char),strlen(url)+strlen(current_url));
						strncpy(links.line_url[links.num],current_url,strlen(current_url)-2);
						strcat(links.line_url[links.num],url);
					}
				}
				links.num++;
				free(url);
			}
		}
		else
			trigger++;
		line_count++;
	}
	while(trigger < 2);
	
	return links;
}

void free_link_info(link_info *linfo)
{
	for(int i = 0; i < linfo->num; i++)
			free(linfo->line_url[i]);
	free(linfo->line_url);
	free(linfo->line_num);
}

void push_link_history(link_history **history, char *url)
{
	if((*history)->url[0] == 'g')
	{
		link_history *prev = (*history);
		(*history) = calloc(1,sizeof(link_history));
		(*history)->prev = prev;
	}
    strncpy(&((*history)->url),url,strlen(url));
}

void pop_link_history(link_history **history)
{
	if((*history) != NULL)
	{
		link_history *prev = (*history)->prev;
		free((*history));
		(*history) = prev;
	}
}

void free_link_history(link_history **history)
{
	while((*history)->prev != NULL)
		pop_link_history((*history));
	pop_link_history((*history));
}
