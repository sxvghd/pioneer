SRCS = *.c
HDRS = *.h
INCS = -lmbedtls -lmbedcrypto -lmbedx509 -lcurses

all:
	${CC} -o pioneer ${SRCS} ${HDRS} ${INCS}
debug:
	${CC} -g -o pioneer ${SRCS} ${HDRS} ${INCS}
