#include <stdlib.h>
#include <string.h>
#include "tls.h"

#define bail(conf_struct, errcode) {\
			tls_deinit(conf_struct);\
			return errcode;\
		}

int tls_init(tlsconfig *conf_struct)
{
	//general init
    mbedtls_ssl_init(&(conf_struct->ssl));
    mbedtls_ssl_config_init(&(conf_struct->conf));
    mbedtls_x509_crt_init(&(conf_struct->cacert));
	mbedtls_ctr_drbg_init(&(conf_struct->ctr_drbg));
	mbedtls_entropy_init(&(conf_struct->entropy));

	//seed
	if((mbedtls_ctr_drbg_seed(&(conf_struct->ctr_drbg), mbedtls_entropy_func, &(conf_struct->entropy),NULL,0 )) != 0)
		bail(conf_struct,1);

	//cert init/load
	if(mbedtls_x509_crt_parse(&(conf_struct->cacert), (const unsigned char *) mbedtls_test_cas_pem,mbedtls_test_cas_pem_len) < 0)
		bail(conf_struct,2);

	if((mbedtls_ssl_config_defaults(&(conf_struct->conf),MBEDTLS_SSL_IS_CLIENT,MBEDTLS_SSL_TRANSPORT_STREAM,MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
        return 2;
	mbedtls_ssl_conf_authmode(&(conf_struct->conf), MBEDTLS_SSL_VERIFY_OPTIONAL);
    mbedtls_ssl_conf_ca_chain(&(conf_struct->conf), &(conf_struct->cacert), NULL);
    mbedtls_ssl_conf_rng(&(conf_struct->conf), mbedtls_ctr_drbg_random, &(conf_struct->ctr_drbg));
	if((mbedtls_ssl_setup(&(conf_struct->ssl), &(conf_struct->conf))) != 0)
        return 3;

	return 0;
}

void tls_deinit(tlsconfig *conf_struct)
{
	mbedtls_net_free(&(conf_struct->server_fd));
    mbedtls_x509_crt_free(&(conf_struct->cacert));
    mbedtls_ssl_free(&(conf_struct->ssl));
    mbedtls_ssl_config_free(&(conf_struct->conf));
    mbedtls_ctr_drbg_free(&(conf_struct->ctr_drbg));
    mbedtls_entropy_free(&(conf_struct->entropy));
	return;
}

int tls_connect(tlsconfig *conf_struct, char *host, char *port)
{
	mbedtls_net_init(&(conf_struct->server_fd));
	//connect
	if((mbedtls_net_connect(&(conf_struct->server_fd), host, port, MBEDTLS_NET_PROTO_TCP)) != 0)
		return 1;

	/*//tls setup
	if((mbedtls_ssl_config_defaults(&(conf_struct->conf),MBEDTLS_SSL_IS_CLIENT,MBEDTLS_SSL_TRANSPORT_STREAM,MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
        return 2;
	mbedtls_ssl_conf_authmode(&(conf_struct->conf), MBEDTLS_SSL_VERIFY_OPTIONAL);
    mbedtls_ssl_conf_ca_chain(&(conf_struct->conf), &(conf_struct->cacert), NULL);
    mbedtls_ssl_conf_rng(&(conf_struct->conf), mbedtls_ctr_drbg_random, &(conf_struct->ctr_drbg));
	if((mbedtls_ssl_setup(&(conf_struct->ssl), &(conf_struct->conf))) != 0)
        return 3;
    */if((mbedtls_ssl_set_hostname(&(conf_struct->ssl), host)) != 0)
        return 4;
    mbedtls_ssl_set_bio(&(conf_struct->ssl), &(conf_struct->server_fd), mbedtls_net_send, mbedtls_net_recv, NULL);
	
	//handshake
	int ret;
	while((ret = mbedtls_ssl_handshake(&(conf_struct->ssl))) != 0)
    {
        if(ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
            return 5;
    }
	//TODO: cert verification (TOFU)	
	return 0;
}

int tls_disconnect(tlsconfig *conf_struct)
{
	mbedtls_net_free(&(conf_struct->server_fd));
    mbedtls_ssl_session_reset(&(conf_struct->ssl));
	return 0;
}

int tls_write(tlsconfig *conf_struct, char *buf)
{
	int ret;
	while((ret = mbedtls_ssl_write(&(conf_struct->ssl), buf, strlen(buf))) <= 0)
    {
        if(ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE )
            return 1;
    }
	return 0;
}

char *tls_read(tlsconfig *conf_struct)
{
	char *response = NULL;
	char *buf = malloc(sizeof(char)*1024);
	int ret, len = 0;
	do
    {
        //len = sizeof(buf) - 1;
        memset(buf, 0, sizeof(char)*1024);
        ret = mbedtls_ssl_read(&(conf_struct->ssl), buf, sizeof(char)*1023);

        if(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE)
            continue;
        if(ret == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY)
            break;
        if(ret < 0)
			return NULL;
        if(ret == 0)
			break;
		response = realloc(response,(sizeof(char)*len)+(1024*sizeof(char)));
		if(len == 0)
			strcpy(response, buf); 
		else
			strcat(response, buf);
		len += 1024;
	}
    while(1);
	free(buf);
	//return new buffer length
	return response;
}

void tls_download(tlsconfig *conf_struct, FILE *file)
{
    char buf;
    int ret;
    do
    {
        buf = 0;
		ret = mbedtls_ssl_read(&(conf_struct->ssl), &buf, sizeof(char));

        if(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE)
            continue;
        if(ret == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY)
            break;
        if(ret < 0)
            return;
        if(ret == 0)
            break;
    	fwrite(&buf,sizeof(char),1,file);
	}
    while(1);
	return;
}

char *tls_read_delim(tlsconfig *conf_struct, char delim)
{
	char *response = calloc(1024,sizeof(char));
	char buf = '\0';
	int ret = 0, len = 0;
	do
    {
		buf = '\0';
        ret = mbedtls_ssl_read(&(conf_struct->ssl), &buf, sizeof(char));

        if(ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE)
            continue;
        if(ret == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY)
            break;
        if(ret < 0)
			return NULL;
        if(ret == 0)
			break;
		response[len] = buf;
		if(buf == delim)
			break;
		len++;
	}
    while(1);
	//return new buffer length
	return response;
}
